## txtdb
 - txtsql - serialize
 - txtDB API - implode
 - txtdb - implode
 - Simple-TxtDb - json
 - axgle - serialize
 - cqiusql - include

## 介绍三种文本数据库：txtsql、txtDB API、txtdb
author: https://aslibra.com/blog/read.php/710.htm


之前希望做一些小项目不用mysql数据库的，而使用的bo-blog又更改性不好，很多结构都是在程序里面写好的，然后想到用php写一个简单的兼容mysql部分语句的类。后来想了一下，估计跟我一个想法的人有很多，就开始查了一下相关的网页。

先查到CTB文本论坛，就是用文本数据库的。没有细看代码，但应该没有兼容mysql语句，里面代码很工整，但好像是直接针对文本操作，可能不是我理想中的样子。后来查到txtdb，是数据库的类。国内有一个叫"PHP Text DataBase API "，国外有一个德国网站的吧，叫"PHP Text DB API"。最后就是txtsql，蛮好的。

下面简单介绍一下：

### 一：txtSQL (2.2 Final  @ 2005-03-26 )
http://txtsql.sourceforge.net/site/index.php


详细介绍这里就不摘取了，见官方网站，安装说明可以参考本站摘录，看完也可以了解关于它的知识：
http://aslibra.com/blog/index.php?job=art&articleid=a_20070401_233244

文件分布方式类似mysql，兼容部分sql语句，还有一个类似mysqlAdmin的txtSQLAdmin

	特色：主键索引、指针定位，跟wwwc说的一样，文件达到20M，速度也很快。
	和上次的结构和算法完全不同了，真正的指针定位，用limit时也是。
	速度测试test库的tbl表，31个字段8000多条记录，翻页时速度0.0几秒。



### 二：PHP Text DB API (0.3.1-Beta-01 @ 2005-02-25)
http://www.c-worker.ch/txtdbapi/index_eng.php
(分隔符)

	SurfChen:
	Txt DB API是一个基于SQL语法的文本数据库。这个文本数据库仅支持最基本的数据库操作和少量的函数，如果你想使用如MYSQL中那么丰富的函数，Txt DB API并不适合你。但是如果你想构建一些比较简单的基于文本的PHP程序，Txt DB API会让你的开发更加轻松愉快。

	Txt DB API最吸引人的是，它支持PEAR::DB这个强大的数据库抽象类。熟悉PEAR::DB的朋友就可以免去阅读Txt DB API文档的工序了。
	当然，不想用PEAR::DB的话，可以使用TxtDBapi本身的数据库类。
	以上SurfChen的文字出处: http://www.surfchen.org/?p=88



### 三：PHP Text DataBase(0.4.0.8 @ 2006-08-18)
http://www.3ants.org/dev/txtdb/

txtdb 是由php编写的一个 csv 格式文本数据库接口类. 简单,高效. 适合运用于小型的 web 系统开发. 以及一些小应用上的开发. 比如 count, refer, log 等. 
txtdb 为开源项目. 您可以免费使用于任何地方.

	SurfChen:
	比上面的TXT DB API更简单，只有一个类。
	这个数据库不支持SQL语法，也没有自增字段等功能，不支持PEAR::DB，支持缓存。
	虽然从感情上支持国人开发的TXT DB，但是个人觉得还是TXT DB API比较好。


引述一下关于文本数据库的说明，希望选择文本数据库的时候可以参考一下：

	优点：
	跨平台，比任何数据库都跨平台；免费，说的是不用再去卖数据库空间；可以像ASP+ACCESS一样管理文件，比如给别人提供程序，或在自己机子调试然后要传到虚拟空间，或换了一个空间要全部移走，就不用phpMyAdmin导出SQL文件再导入；不用出现乱码，MySQL出现乱码的情况应该都遇到过，当然那是有办法解决的，但用文本数据库是不会出现乱码的
	缺点：
	速度不如专业的数据库，也不支持SQL语句，

为什么使用文本数据库：
新鲜，好玩，挑战性，想尝试一下，拓展思路，
除了大型BBS多用户统计多用户BLOG外，很多程序我们都可以生成静态来解决速度的问题，静态网页的速度最快，而且被搜索引擎收录的机会更大了。

资源：
- http://www.3ants.org/txtdb/        TXTDB类
- http://mixer.shakeme.net/cn/        一个用TXTDB的blog
- http://www.bo-blog.com/index2.php        bo-blog的旧版是采用文本数据库的
- http://axgle.php5.cz/rar/bbsba/        axgle的文本贴吧
- http://www.ofstar.net/                Ofstar Board
- http://ctb.isyi.com/                CTB文本论坛



另外，有一个SQL解释器不错，就是把sql语句转换操作
需要的可以去看看，自己做兼容sql语句的程序可以用到哦！
http://www.freediscuz.net/bbs/viewthread.php?tid=332
