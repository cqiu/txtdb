<?php
include dirname(__File__) . '/txtdb.rear.inc.php';
/**
 * @desc txtdb with debug console
 * @author episome <eipsome(at)gmail.com>
 * @link http://www.3ants.org
 * @copyright The Three Ants
 */
class txtdb_debugger extends txtdb_rear{
	/**
	* @desc debug console
	* @param bool $pop
	* @return void
	*/
	function debug($pop = false){
		$_console_css_style = '
		<style>
			.txtdb_debuger{
				background: #F9F9F9;
				border: 1px solid silver;		
				padding:13 px;
				font: 12pt "Courier New";
				text-align: left;
			}
			.txtdb_debuger #logo{
				text-decoration: none;
				font-size:14pt;
				color:darkblue;
				font-weight: bold;
			}	
			.txtdb_debuger #version{
				font-size:9pt;
				color:blue;
			}				
			.txtdb_debuger HR{
				border:thin silver #F9F9F9;
			}
			.txtdb_debuger DD{
				table-layout: fixed;
				padding-left:13px; 
				margin-left:13px; 
				border-left:1px white ridge;
			}			
			.txtdb_debuger STRONG{
				color:darkblue;
			}	
			.txtdb_debuger B{
				color:darkred;
			}
			.txtdb_debuger Q{
				color:magenta;
			}	
			.txtdb_debuger I{
				color:orange;
			}	
			.txtdb_debuger #table{
				color:limegreen;
			}
		</style>								
		';
		$_console_html_body  = '
		<b id=logo>TxtDb Debug Console</b> <sup id="version">'.TXTDB_VERSION.'</sup><br>
		<hr size=1>
		<dl>
			<dt><strong>Server Info:</strong></dt>
			<dd><b>Server:</b>' . $_SERVER['SERVER_SOFTWARE'] . '</dd>
		</dl>
		<dl>
			<dt><strong>txtDb Info:</strong></dt>
			<dd><b>exten:</b>' . $this->exten . '</dd>
			'.$this->_display_array($this->db).'
		</dl>		
		';
		foreach((array)$this->tbls as $table=>$tableValues){
			$this->tbls[$table]['header'] = $this->db['denyMsg'] . TXTDB_SEPARATOR . $this->tbls[$table]['rows'] . TXTDB_SEPARATOR . $this->tbls[$table]['auto_id'] . TXTDB_SEPARATOR . $this->tbls[$table]['max_len'] . TXTDB_SEPARATOR . @implode(TXTDB_SEPARATOR, $this->tbls[$table]['fields']);
			$_console_html_body .= '
		<hr size=1>	
		<dl>
			<dt><strong>table [ <a id="table">' . $table . '</a> ] info:</strong></dt>
			'.$this->_display_array($tableValues).'
		</dl>
		';
		}
		$_console_html_body .= '<hr size=1><a href="http://www.3ants.org/txtdb" target="_blank" id="logo">Copyright: Three Ants</a>';
		if(!$pop){
			echo "<div class='txtdb_debuger'>";
			echo str_replace('\n', "\n", $_console_css_style.$_console_html_body);
			echo "</div>";
		}else{
			$_console_html_header = '<html>\n';
			$_console_html_header .= '<title>TxtDb Debug Console</title>\n';
			$_console_html_header .= $_console_css_style;
			$_console_html_header .= '<body class="txtdb_debuger">\n';
			$_console_html_footer .= '</body>\n';
			$_console_html_footer .= '</html>\n';
			$_console_html = str_replace("\n",'',$_console_html_header.$_console_html_body.$_console_html_footer);
			print"
		<script>
			txtDb_Console=window.open('','','toolbar=no,resizable=yes,scrollbars=yes,width=450,height=600');
			txtDb_Console.document.write('$_console_html');
			txtDb_Console.document.close();
			txtDb_Console.focus();
		</script>";
		}
	}
	// private function to display an array;
	function _display_array($array, $level = 0){
		$_result = '';
		foreach($array as $key => $value){
			switch (gettype($value)) {
				case 'boolean':
				$value = $value ? '<i>true</i>' : '<i>false</i>';
				$_result .= '<dd><nobr><b>' . $key . ':</b>' . $value . '</nobr></dd>\n';
				break;
				case 'array':
				$_result .= '<dl><dt><b>' . $key . ': </b></dt>\n' . $this->_display_array($value, $level + 1) . '</dl>';
				break;
				default:
				$_result .= '<dd><nobr><b>' . $key . ':</b>' . str_replace(array("<", "'", "&#10", "\r", "&#13", TXTDB_SEPARATOR),array("&lt;", "&#39", '&#92n', '&#92r', '&#92r', '<Q>' . TXTDB_SEPARATOR . '</Q>'), $value) . '</nobr></dd>\n';
				break;
			}
		}
		return $_result;
	}
}