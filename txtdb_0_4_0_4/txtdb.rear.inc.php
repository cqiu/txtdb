<?php 
include_once dirname(__File__) . '/txtdb.inc.php';
/**
 * @desc txtdb with sync,field_drop,field_append,field_del,field_rename.
 * @author episome <eipsome(at)gmail.com>
 * @link http://www.3ants.org
 * @copyright The Three Ants
 */
class txtdb_rear extends txtdb{
	/**
	 * @descc sync table
	 * @param string $table
	 * @return void
	 */
	function sync($table = null){
		$this->_check_table($table,true);
		$this->tbls[$table]['rows'] = count($this->tbls[$table]['records']);
		$this->tbls[$table]['maximum'] = strlen($this->tbls[$table]['header']) + 10;
		$syncedRecords = array();
		for($id = 0;$id < $this->tbls[$table]['rows'];$id++){
			$this->_build_values($id, $table);
			$syncedRecords[$id] = implode(TXTDB_SEPARATOR, $this->tbls[$table]['values'][$id]);
			$this->tbls[$table]['maximum'] = max($this->tbls[$table]['maximum'], strlen($syncedRecords[$id]) + 10);
		}
		$this->tbls[$table]['records'] = &$syncedRecords;
	}
	/**
	 * @desc drop a field
	 * @param string $field
	 * @param string $table
	 * @return bool
	 */
	function field_drop($field, $table = null){
		$this->_check_table($table,true);
		if(!in_array($field, $this->tbls[$table]['fields'])){
			return false;
		}
		if($this->cols($table)==1){
			$this->_halt('You can\'t drop last field',false);
			return false;
		}
		$field = trim($field);
		foreach($this->tbls[$table]['fields'] as $key => $f){
			if(trim($f) == $field){
				unset($this->tbls[$table]['fields'][$key]);
				break;
			}
		}
		$this->tbls[$table]['fields'] = array_values($this->tbls[$table]['fields']);
		foreach($this->tbls[$table]['records'] as $id => $line){
			$record = explode(TXTDB_SEPARATOR, $line);
			unset($record[$key]);
			$this->tbls[$table]['records'][$id] = implode(TXTDB_SEPARATOR, $record);
		}
		$ids = array_keys($this->tbls[$table]['values']);
		foreach($ids as $id){
			unset($this->tbls[$table]['values'][$id][$field]);
		}
		$this->tbls[$table]['header'] = $this->db['denyMsg'] . TXTDB_SEPARATOR . $this->tbls[$table]['rows'] . TXTDB_SEPARATOR . $this->tbls[$table]['auto_id'] . TXTDB_SEPARATOR . $this->tbls[$table]['maximum'] . TXTDB_SEPARATOR . @implode(TXTDB_SEPARATOR, $this->tbls[$table]['fields']);
		$this->tbls[$table]['cols'] = count($this->tbls[$table]['fields']);
		return true;
	}
	/**
	* @desc append a field
	* @param string $field
	* @param string $table
	* @return bool
	*/
	function field_append($field, $table = null){
		$this->_check_table($table,true);
		$field = trim($field);
		if(in_array($field, $this->tbls[$table]['fields'])){
			$this->_halt('Field [' . $field . '] already in table [' . $table . ']');
		}
		if(!$this->_check($field)){
			$this->_halt('Field name [' . $field . '] not allowed!');
		}
		array_push ($this->tbls[$table]['fields'], $field);
		$this->tbls[$table]['header'] = $this->db['denyMsg'] . TXTDB_SEPARATOR . $this->tbls[$table]['rows'] . TXTDB_SEPARATOR . $this->tbls[$table]['auto_id'] . TXTDB_SEPARATOR . $this->tbls[$table]['maximum'] . TXTDB_SEPARATOR . @implode(TXTDB_SEPARATOR, $this->tbls[$table]['fields']);
		$this->tbls[$table]['maximum'] = max($this->tbls[$table]['maximum'], strlen($this->tbls[$table]['header']) + 10);
		$ids = array_keys($this->tbls[$table]['records']);
		if($field == $this->auto_id_name){
			$this->tbls[$table]['auto_id'] = 1;
			foreach($ids as $id){
				$this->tbls[$table]['records'][$id] = $this->tbls[$table]['records'][$id].TXTDB_SEPARATOR.$this->tbls[$table]['auto_id'];
				$this->tbls[$table]['auto_id']++;
			}
		}else{
			foreach($ids as $id){
				$this->tbls[$table]['records'][$id] = $this->tbls[$table]['records'][$id].TXTDB_SEPARATOR;
			}
		}
		$this->tbls[$table]['cols'] = count($this->tbls[$table]['fields']);
		return true;
	}
	/**
	* @desc rename a field
	* @param string $source
	* @param string $target
	* @param string $table 
	* @return bool
	*/
	function field_rename($source, $target, $table = null){
		$this->_check_table($table,true);
		$source = trim($source);
		$target = trim($target);
		if($source == $target) return true;
		if(!in_array($source, $this->tbls[$table]['fields'])){
			$this->_halt('Source field name [' . $source . '] not exists!');
		}
		if(in_array($target, $this->tbls[$table]['fields'])){
			$this->_halt('Target field name [' . $target . '] already exists!');
		}
		if(!$this->_check($target) || $target == $this->auto_id_name){
			$this->_halt('Target field name [' . $target . '] not allowed!');
		}
		$this->tbls[$table]['fields'] = preg_replace("/^($source){1}$/" , $target , $this->tbls[$table]['fields']);
		$this->tbls[$table]['header'] = $this->db['denyMsg'] . TXTDB_SEPARATOR . $this->tbls[$table]['rows'] . TXTDB_SEPARATOR . $this->tbls[$table]['auto_id'] . TXTDB_SEPARATOR . $this->tbls[$table]['maximum'] . TXTDB_SEPARATOR . @implode(TXTDB_SEPARATOR, $this->tbls[$table]['fields']);
		$this->tbls[$table]['maximum'] = max($this->tbls[$table]['maximum'], strlen($this->tbls[$table]['header']) + 10);
		unset($this->tbls[$table]['values']);//:fix:
		return true;
	}
}