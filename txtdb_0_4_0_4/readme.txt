PHP text database API.
------------------------------
PHP text database API 是一个提供PHP对文本csv文件操作的应用接口，
包括表操作，记录操作，字段操作等功能函数。

文档查询：
http://txtdb.3ants.org/manual.php

目前使用PHP text database API的网站有：
http://www.3ants.org
http://www.shakeme.net
http://vifix.ihfs.net
http://hino.ihfs.net
http://zzmango.com/bbs

联系:
QQ: 13008018
Msn: episome@gmail.com
Google Talk: episome@gmail.com