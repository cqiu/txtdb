<?php
/*
** TxtDb GUI Dev
** author: episome <webmaster(at)3ants.org>
** This script is the GUI manager for PHP txtDb.
** lastmodify: 2005-05-09 9:42
***/
// config
$set['admin']      = 'admin';
$set['password']   = 'password';
$set['database']   = '../database/';
$set['tmp']        = '../../_tmp/';
$set['charset']    = 'gb2312';
$set['cache']      = false;
$set['numPrePage'] = 10;
$set['strLimit']   = 100;
// authenticate
/*if ($_SERVER['PHP_AUTH_USER'] != $set['admin'] || $_SERVER['PHP_AUTH_PW'] != $set['password']){
	header('WWW-Authenticate: Basic realm="PHP Text Database API GUI"');
	die("<b style='padding:30px;color:red;font-size:13pt;font-family:Courier New;'>Access denied !!!</b>");
}*/
// send header
@header("Cache-control: private");
@header("Content-Type: text/html; charset=".$set['charset']);
@ob_start();
include("../txtdb.rear.inc.php");
// var
$_POST = unquotes($_POST);
$_GET = unquotes($_GET);
if(!$set['cache']){
	$set['tmp'] = null;
}
// GUI defined.
class GUI extends txtdb_rear{

	public function __construct()
	{
		$this->GUI();
	}
	// GUI constructor
	function GUI(){
		global $set;
		$parameter = explode(',',$_SERVER['QUERY_STRING']);
		$module = array_shift($parameter);
		if(empty($module)){
			$module = 'frames';
		}elseif(!method_exists($this,$module)){
			if(!method_exists($this,$module)) echo "<script>alert('Unknow module:\\n",$module,"')</script>";
			$module = 'welcome';
		}
		$this->pHeader();
		$this->$module($parameter);
		$this->pFooter();
	}
	// print header
	function pHeader(){
		print'
<html>
<head>
<title>PHP Text Database API GUI</title>
<meta http-equiv=pragma content=no-cache>
<meta http-equiv=no-cache>
<meta http-equiv=expires content=-1>
<meta http-equiv=cache-control content=no-cache>
<meta name=\"robots\" content=\"noindex, nofollow\">
<style>
	/*global*/
	body{margin:0px;}
	*{font:9.8pt \"courier new\";}
	hr{color:#336699;}	
	a,a:visited,a:hover{cursor:pointer;text-decoration:none;color:#00008B;}
	a:hover{color:red;}
	form{margin:0}
	li{margin:0px;list-style-type:none;padding-left:7px;line-height:21px;list-style:none;}
	.text{color:gray}
	.text_changed{color:black}

	.title{font-size: 13pt;color: #00008b;padding-left: 3pt;line-height: 230%;font-weight: bold;cursor: pointer;}

	#logoFrame{background:#809BCB;margin:3px 5px;filter: Alpha(Opacity=100, FinishOpacity=60, Style=1, StartX=0, StartY=100, FinishX = 0, FinishY = 0);}
	#logoDiv{font-size: 11pt;font-weight: bold;color: white;padding: 1px 5px;background: #00008B;border: 1px solid white;}
	#welcome{margin:100px;}
	#footerFrame{overflow:hidden;background:#809BCB;margin:7px;filter: Alpha(Opacity=100, FinishOpacity=60, Style=1, StartX=0, StartY=0, FinishX = 0, FinishY = 100);}

	/*navigation*/
	#navigation {color:white;margin:7px 7px 0px 7px;scrollbar-face-color:#809bcb;scrollbar-highlight-color:#809bcb;scrollbar-shadow-color:#809bcb;scrollbar-arrow-color: white;scrollbar-base-color: #a2d0ff;scrollbar-dark-shadow-color:#336699;overflow-x:hidden;background:#809bcb;}
	#navigation ul{list-style:none;margin:0px;padding-left:7px;}
	#navigation li{line-height: 17px;border-left:#00008b 3px solid;}
	#navigation a:hover{color:white;text-decoration: underline;}
	#navigation a{color:#00008b;white-space: nowrap;text-decoration: none;}
	#navigation .dir{font-weight: bold;color:#00008b;}
	#navigation .dir_highlight{font-weight: bold;color:#00008b;}
	#navigation .table{color:#00008b;}
	#navigation .table_highlight{color:aqua;}

	/*data list*/
	.tbldata thead tr.head td,.tbldata tfoot tr.foot td{color:white;text-align: left;height:37px;padding-left: 7px;background-color: #7694C8;filter: Alpha(Opacity=100, FinishOpacity=50, Style=1, StartX=0, StartY=100, FinishX = 0, FinishY = -50);}
	.tbldata thead tr.cols td,.tbldata tfoot tr.pages td{color: black;background-color:#ccc;height: 24px;font-weight: bold;white-space: nowrap;text-align: left;padding-left:4px;padding-right:4px;border-right: solid 1px white;border-top: solid 2px #B6C7E5;border-bottom: solid 2px #B6C7E5;filter: Alpha(Opacity=100, FinishOpacity=60, Style=1, StartX=0, StartY=100, FinishX = 0, FinishY = -50);}
	.tbldata tbody tr td {color: black;border-right: solid 1px white;border-bottom: solid 2px white;height: 19px;text-align:normal;background-color: #EEE;white-space: nowrap;padding-left:4px;padding-right:4px;}
	.tbldata tbody tr.selected td{color: #8B0000;border-right: solid 1px white;border-bottom: solid 2px #B6C7E5;height: 19px;text-align:normal;background-color: #EEF3FB;white-space: nowrap; }
	.tbldata tbody tr.clicked td{background-color: aqua;}
</style>
<script language = "javascript">
	//if(window == top)window.location.href = "?";
	jump2 = function(url,target){
		if(target!=undefined) target.location.href = url;
			else window.location.href = url;
	}
	extendBox = function(o){ 
		window.box = o;
		var _html = "<title>PHP Text Database GUI</title>"
				 + "<style>body{margin:0px;border:0px;font-size:10pt;font-family:Courier New;}</style>"
				 + "<body scroll=no onunload=\"if(opener){opener.box.value=document.getElementById(\'tbox\').value}\">"
				 + "<textarea id=tbox style=\"width:100%;height:100%\">" + o.value + "</textarea>"
		var win = window.open("","","alwaysRaised=yes,top=230,left=230,height=200,width=400,resizable=yes,location=no,menubar=no,statuebar=no,titlebar=no,toolbar=no "); 
		win.document.write(_html)
		win.document.close(); 
	}
	//window.onerror = function(){return true;};
	inputFocus = function(o){if(o.value==o.title){o.className+="_changed";o.value=""}};
	inputBlur = function(o){if(o.value==""){o.className=o.className.replace("_changed","");o.value=o.title}};
	switchNav = function(){top.document.getElementById(\'frms\').cols = (top.document.getElementById(\'frms\').cols=="0,*")?"133,*":"0,*";}
	var lastActiveLink;
	highlight = function(o){if(lastActiveLink != undefined){lastActiveLink.className = lastActiveLink.className.replace("_highlight","")};o.className += "_highlight";lastActiveLink=o;}
</script>
';
	}
	// print footer
	function pFooter(){
		print"
</html>";
	}
	// welcome
	function welcome(/*$parameter*/){
		global $set;
		$version = TXTDB_VERSION;
		$dbpath = $set['database'];
		$server = $_SERVER["SERVER_SOFTWARE"];
		$php = PHP_VERSION;
		$os = PHP_OS;
		print"
<body onselectstart='return false;' id=welcome>
<div class='title' onclick='switchNav();'>PHP Text Database GUI</div>
<hr size=1>
<li>TxtDb $version </li>
<li>PHP $php running on $os </li>
<li>Server_SoftWare : $server </li>
<li>Database : $dbpath </li>";
	}
	// frames
	function frames(){
		echo '<frameset id="frms" cols="133,*"><frame src="?leftFrames" name="nav"><frame src="?welcome" name="main"></frameset>';	
	}
	// leftFrames
	function leftFrames(){
		echo '<frameset name="frms" rows="29,*,29" border=0><frame src="?logo" scrolling="NO" noresize><frame src="?navigation" name="nav"><frame src="?footer" scrolling="NO" noresize></frameset>';
	}
	// logo frame
	function logo(){
		print("<body onselectstart='return false;' id=logoFrame><div id=\"logoDiv\">txtDb.GUI</div></body>");	
	}
	// footer frame
	function footer(){
		print"
		<body onselectstart='return false;' id=footerFrame>
			<a href=\"http://www.3ants.org/txtdb\" target=\"_blank\" hidefocus><img border=0 src='txtdb.png' width=80 height=15 title=\"Link to txtDb wetsite\"></a>
		</body>
		";	
	}
	// navigation frame
	function navigation($parameter){
		$parameter or $parameter = [0 => ''];
		global $set;
		print"
<body id=navigation>
<script language=\"javascript\">
	if(window == top)window.location.href = '?';
</script>
		";
		$this->connect($set['database'],$set['tmp']);
		print"
			<a href=\"?welcome\" target=\"main\" class='dir' hidefocus onclick=\"highlight(this)\">&lt;Welcome&gt;</a><br>
			<a href=\"?navigation\" onclick=\"jump2('?directory',top.main)\" alt=\"".$this->db['path']."\" class='dir' hidefocus>&lt;root&gt;</a>";
		$parameter[0] = urldecode($parameter[0]);
		if($parameter[0]!='' && substr($parameter[0],-1)!="/") $parameter[0] .= "/";
		echo "<ul>";
		$directory = scanDirectory(urldecode($this->db['path'].$parameter[0]));
		if($parameter[0] != ''){
			$parent = explode("/",$parameter[0]);
			@array_pop($parent);
			@array_pop($parent);
			$parent = urlencode(@join("/",$parent));
			print"
			<li><a href='#' onclick=\"jump2('?directory,$parameter[0]',top.main)\" title='Current directory' class='dir' hidefocus>&lt;.&gt;</a><br>		
			<li><a href='?navigation,$parent' onclick=\"jump2('?directory,$parent',top.main)\" title='Parent directory' class='dir' hidefocus>&lt;..&gt;</a>";		
		}
		foreach ($directory['directory'] as $dir) {
			$encodeDirectory = urlencode($parameter[0].$dir);
			print"
			<li><a href='?navigation,$encodeDirectory' onclick=\"jump2('?directory,$encodeDirectory',top.main)\" title='Dir:$dir' class='dir' hidefocus>&lt;$dir&gt;</a>";				
		}
		$this->extenLen = strlen($this->exten);
		foreach ($directory['file'] as $file) {
			$exten = substr($file,-($this->extenLen));
			if($exten == $this->exten){
				$file = str_replace($exten,'',$file);
				$encodeFile = urlencode($parameter[0].$file);
				print"<li><a href='?Browse,$encodeFile' onclick='highlight(this)' title='Table:$file' class='table' target='main' hidefocus>$file</a>";
			}
		}
	}

	/*
	** Brower table
	**/
	function browse($parameter){
		global $set;
		$this->connect($set['database'],$set['tmp']);
		$encodeTable = $parameter[0];
		$page = empty($parameter[1]) ? 1 : $parameter[1];
		$table = urldecode($encodeTable);
		if(method_exists($this,$_POST['act'])){
			// var_dump($_POST['act']);exit;
			$this->{$_POST['act']}($table,$encodeTable);
		}
		// �����
		$page = (is_null($page)||($page<0))?1:$page;
		$fields  = $this->fields($table);
		foreach($fields as $field){
			$fieldsRows .= "
					<td><a onclick=\"doSort('$field')\">$field</a>&nbsp;</td>";
			$fieldsOptions .= "<option value='$field'>$field";
		}
		$multiPage = '';
		if(trim($_POST['expression']) == '' || trim($_POST['expression'])=='true'){
			$_POST['expression'] = 'true';
			$limit = (($page-1)*$set['numPrePage']).','.$set['numPrePage'];
			$rows = $this->rows($table);
			if($rows > $set['numPrePage']){
				$pages = @ceil($rows/$set['numPrePage']);
				$multiPage = ($page==1) ? "Pages: FIRST PREV " : "Pages: <a onclick=\"jump2page('$encodeTable',1)\">FIRST</a> <a onclick=\"jump2page('$encodeTable',".($page-1).")\">PREV</a> ";
				$min = ($page - 5)<1 ? 1 : $page - 5;
				$max = ($page + 5)>$pages ? $pages : $page + 5;
				$multiPage .= ($min>1) ? "... " : ' ';
				for($i=$min;$i<=$max;$i++) $multiPage.=$i==$page ? "<big><b>".$i."</b></big> " : "<a onclick=\"jump2page('$encodeTable',$i)\">".$i."</a> ";
				$multiPage .= ($max<$pages) ? "... " : ' ';
				$multiPage .= ($page+1)<=$pages ? "<a onclick=\"jump2page('$encodeTable',".($page+1).")\">NEXT</a> " : "NEXT ";
				$multiPage .= ($page<$pages) ? "<a onclick=\"jump2page('$encodeTable',$pages)\">END</a>" : "END";
			}
		}else{
			$limit = null;
		}
		$records = $this->get($_POST['expression'],$limit,$table,$set['cache'],$_POST['sortBy'],$_POST['sortFlag']);
		foreach($records as $id=>$record){
			$recordsRows .= "
				<tr>
					<td><input type=\"checkbox\" name=\"ids[]\" value=\"$id\" hidefocus></td>
					<td nowap><a onclick=\"doEdit($id);\" title=\"Edit $id\">[edit]</a></td>";
			foreach($fields as $field){
				$recordsRows .= "
					<td>".$this->convert($record[$field])."</td>";
			}
			$recordsRows .= "
				</tr>";
		}

		print"
		<script type=\"text/javascript\">
			function DataTable_Init(){
				g_oDataTable = document.getElementById('datatable');
				if(document.getElementById('datatable')){
					var SelectAllRows = document.getElementById('selectallrows');
					var NumberChecked = 0;
					var aCheckBoxes = document.getElementsByName('ids[]');
					var fCheckBox_Click = function(){
						if(this.checked){
							this.oTr.className = 'selected';
							NumberChecked++;
						}else{
							this.oTr.className = '';
							NumberChecked--;
						}
						this.oTr.oClassName = this.oTr.className;
						SelectAllRows.checked = (NumberChecked == aCheckBoxes.length);
						rowclick = false;
					}
					var lastRow;
					var rowclick = true;
					var fClicked = function(){
						if(!rowclick){rowclick = true; return;}
						if(lastRow)lastRow.className = lastRow.oClassName;
						this.oClassName = this.className;
						this.className = 'clicked';
						lastRow = this;
					}
					var aRows = g_oDataTable.tBodies[0].rows;
					var nRows = aRows.length-1;
					for (var i = nRows; i >= 0; i--){
						aCheckBoxes[i].oTr = aRows[i];
						aRows[i].onclick = fClicked;
						aCheckBoxes[i].onclick = fCheckBox_Click;
					}
					var fSelectAllRows_Click = function(){
						for(var i=nRows;i>=0;i--){
							aCheckBoxes[i].checked = this.checked;
							aRows[i].className = aRows[i].oClassName = this.checked ? 'selected' : '';
						}
						NumberChecked = (this.checked) ? aCheckBoxes.length : 0;
					}
					SelectAllRows.onclick = fSelectAllRows_Click;
				}
				else return false;
			};
			 
			function doDelete(){
				if(confirm(\"Do you really want to delete what you select?\")){
					document.dataList.act.value=\"delRecord\"
					document.dataList.submit();
				}
				return ;
			}
			function doAppend(){
				dataList.act.value='insertRecord';
				dataList.submit()
			}
			function doEdit(i){
				dataList.act.value='editRecord';
				dataList.id.value=i;
				dataList.submit();
			}
			function doClear(){
				if(window.confirm('Do you really want to empty the table?')){
					dataList.act.value='doClear';
					dataList.submit()
				}
			}
			function doSync(){
					dataList.act.value='doSync';
					dataList.submit()
			}
			function doDrop(){
				if(window.confirm('Do you really want to drop the table?')){
					dataList.act.value='dropTable';
					dataList.submit()
				}
			}
			function doSort(field){
				dataList.act.value='';
				dataList.sortBy.value = field;
				dataList.sortFlag.value = dataList.sortFlag.value=='asc' ? 'desc' : 'asc';
				dataList.submit()
			}
			function doQuery(){	
				dataList.act.value='';
				dataList.id.value='';
				dataList.submit();
			}
			function doNewField(){
				if(dataList.newFieldName.value==''){
					alert(\"The new field name no set!\");
					return false;
				}
				dataList.act.value='newField';
				dataList.submit()
			}
			function doRenameField(){
				if(dataList.newFieldName.value==''){
					alert(\"The new field name no set!\");
					return false;
				}
				if(window.confirm('Do you really want to rename the field:'+dataList.fieldName.value+' to '+dataList.newFieldName.value+'?')){
					dataList.act.value='renameField';
					dataList.submit();
				}			
			}
			function doDropField(){
				if(window.confirm('Do you really want to drop the field:'+dataList.fieldName.value+'?')){
					dataList.act.value='dropField';
					dataList.submit();
				}			
			}
			function jump2page(t,i){
				dataList.act.value='';
				dataList.action='?browse,'+t+','+i;
				dataList.submit();
			}
		</script>
		<body>
		<form name=\"dataList\" method=\"post\" action=\"?browse,$encodeTable,$page\">
		<input type=\"hidden\" name=\"act\" value=\"\">
		<input type=\"hidden\" name=\"id\" value=\"\">
		<input type=\"hidden\" name=\"sortBy\" value=\"".$_POST['sortBy']."\">
		<input type=\"hidden\" name=\"sortFlag\" value=\"".$_POST['sortFlag']."\">
		<b class=\"title\" onclick=\"switchNav();\">Table:$table</b>
		<table cellpadding=0 cellspacing=0 width=\"100%\" id=\"datatable\" class=\"tbldata\">
			<thead>
				<tr class=\"head\">
					<td colspan=99>
						<input name=\"expression\" value=\"".htmlspecialchars($_POST['expression'])."\" size=33>
						<input type=\"button\" value=\"Query\" class=\"button\" onclick=\"doQuery();\" hidefocus>
					</td>
				</tr>
				<tr class=\"cols\">
					<td width=1%><input type=\"checkbox\" id=\"selectallrows\" name=\"toggleAll\" title=\"Select all or none\" hidefocus></td>
					<td width=1%>[act]</td>
					$fieldsRows
				</tr>
			</thead>
			<tbody>
				$recordsRows
			</tbody>
			<tfoot>".(trim($multiPage)!='' ?
				"<tr class=\"pages\">
					<td colspan=\"999\" nowrap> $multiPage</td>
				</tr>":"")
				."<tr class=\"foot\">
					<td colspan=\"999\">
						<input type=\"button\" value=\"Delete\" title=\"Delete selected\" class=\"button\" onclick=\"doDelete();\" hidefocus>
						<input type=\"button\" value=\"Append\" title=\"Append a record\" class=\"button\" onclick=\"doAppend();\" hidefocus>
					</td>
				</tr>
			</tfoot>
		</table>
		<li><b>Path :</b> {$this->tbls[$table]['path']}
		<li><b>Last modification :</b> ".date ("F d Y H:i:s", $this->get_table_mtime($table))."
		<li><b>Size :</b> ".number_format($this->get_table_size($table))." (bytes)	
		<li><b>Records :</b> ".$this->rows($table)."
		<li><b>Fields :</b> ".$this->cols($table)."
		<li><b>Maximum record :</b> ".($this->tbls[$table]['maximum']-10)." byte.
		<li><select size=1 style='width:100' name=\"fieldName\">
				$fieldsOptions
			</select>
			<input name=\"newFieldName\" style='width:100'>
			<input type='button' value='new field' class='button' hidefocus onclick=\"doNewField()\">
			<input type='button' value='rename field' class='button' hidefocus onclick=\"doRenameField()\">
			<input type='button' value='drop field' class='button' hidefocus onclick=\"doDropField()\">
		<li><a onclick=\"doClear()\"><b>Clear</b></a> or <a onclick=\"doDrop();\"><b>Drop</b></a> or <a onclick=\"doSync()\"><b>Sync</b></a> this table.
		</form>
		<br>
		<br>
		<script language=\"JavaScript\">
			DataTable_Init();
		</script>
		";
	}
	// rename filed
	function renameField($table){
		$this->field_rename($_POST['fieldName'],$_POST['newFieldName'],$table);
		$this->save($table);
	}
	// new filed
	function newField($table){
		$this->field_append($_POST['newFieldName'],$table);
		$this->save($table);
	}
	// drop Field
	function dropField($table){
		$this->field_drop($_POST['fieldName'],$table);
		$this->save($table);
	}
	// edit record
	function editRecord($table,$encodeTable){
		global $_POST;
		if(is_array($_POST['values'])){
			$this->set($_POST['values'],$_POST['id'],null,$table);
			if($_POST['newId']!=$_POST['id']){
				$this->shift($_POST['id'],$_POST['newId'],$table);
			}
			$this->save($table);
			return;
		}
		list($id,$record) = each($this->get($_POST['id'],null,$table));
		foreach($this->fields($table) as $field){
			$disabled = $field == $this->auto_id_name ? ' disabled' : '';
			$fieldsRows .= "
			<tr>
				<td align='right'><nobr>$field :&nbsp;</nobr></td>
				<td><textarea name='values[$field]' rows=4 style='width:411' $disabled ondblclick='extendBox(this)'>".$record[$field]."</textarea></td>
			</tr>";
		}
		print"
		<body>
		<form name=\"dataList\" method=\"post\" action=\"?browse,$encodeTable\">
		<input type=\"hidden\" name=\"act\" value=\"editRecord\">
		<input type=\"hidden\" name=id value=\"$id\">
		<b class=\"title\" onclick=\"switchNav();\">Table:$table</b>
		<table cellpadding=0 cellspacing=0 width=\"100%\" class=\"tbldata\">
			<thead>
				<tr class=\"head\">
					<td colspan=2>Change an record.</td>
				</tr>
			</thead>
			<tbody>
				$fieldsRows
			</tbody>
			<tfoot>
				<tr class=\"foot\">
					<td nowrap colspan=2>Row id:<input size=3 name=newId value=\"$id\">&nbsp;<input type=submit onclick='return !isNaN(dataList.newId.value);' value='Save' class=button hidefocus>&nbsp;<input hidefocus type='button' value='Cancel' class=button onclick='js:history.go(-1)'></td>
				</tr>
			</tfoot>
		</table>
		</form>";		
		die();
	}
	/*
	** delete record
	******************/
	function delRecord($table){
		global $_POST;
		$exp = @implode(',',$_POST['ids']);
		$this->del($exp,null,$table);
		$this->save($table);
	}
	/*
	** insert record
	******************/
	function insertRecord($table,$encodeTable){
		global $_POST;
		if(is_array($_POST['values'])){
			$this->append($_POST['values'],$_POST['id'],$table);
			$this->save($table);
			return;
		}
		foreach($this->fields($table) as $field){
			if($field == $this->auto_id_name){
				continue;
			}
			$fieldsRows .= "
			<tr>
				<td align='right'><nobr>$field :&nbsp;</nobr></td>
				<td><textarea name='values[$field]' rows=4 style='width:411' ondblclick='extendBox(this)'></textarea></td>
			</tr>";
		}
		print"
		<body>
		<form name=\"dataList\" method=\"post\" action=\"?browse,$encodeTable\">
		<input type=\"hidden\" name=\"act\" value=\"insertRecord\">
		<b class=\"title\" onclick=\"switchNav();\">Table:$table</b>
		<table cellpadding=0 cellspacing=0 width=\"100%\" class=\"tbldata\">
			<thead>
				<tr class=\"head\">
					<td colspan=2>Insert an record.</td>
				</tr>
			</thead>
			<tbody>
				$fieldsRows
			</tbody>
			<tfoot>
				<tr class=\"foot\">
					<td nowrap colspan=2>Row id:<input size=3 name=id value=".$this->rows($table)." onkeypress =\"return new RegExp('48|49|50|51|52|53|54|55|56|57','ig').test(event.keyCode);\" onpaste=\"return false;\">&nbsp;<input type=submit value='Insert' class=button>&nbsp;<input type=button value='Cancel' class=button onclick='js:history.go(-1)'></td>
				</tr>
			</tfoot>
		</table>
		</form>";
		die();
	}
	// convert
	function convert($string){
		global $set;
		if(strlen($string)>$set['strLimit']){
			$string=substr($string,0,$set['strLimit']).' <font color=#8b0000>...</font>';
		}
		$string = htmlspecialchars($string);
		$string = str_replace(array("\n","\r"),array("<font color=\"#FF0000\">&#92n</font>","<font color=#8B0000>&#92r</font>"),$string);
		return $string;
	}
	// drop table
	function dropTable($table){
		$this->drop($table);
		$_arr = split('/',$table);
		array_pop($_arr);
		$dir = join('/',$_arr);
		die("<script>top.nav.nav.location.reload();jump2('?directory,$dir');</script>");
	}
	// clear table
	function doClear($table){
		$this->clear($table);
		$this->save($table);
	}
	// sync table
	function doSync($table){
		$this->sync($table);
		$this->save($table);
	}
	/*
	** brower directory
	*********************/
	function directory($parameter) {
		global $set;
		$this->connect($set['database'],$set['tmp']);
		$encodeDir = $parameter[0];
		$dir = urldecode($parameter[0]);
		if(method_exists($this,$_POST['act'])){
			$this->$_POST['act']($dir,$encodeDir);
		}
		// brower directory
		print"
		<body>
		<script language=\"JavaScript\">
		function doNewTable(){
			result = isNaN(dataList.tableName.value) && isNaN(dataList.fields.value);
			if(result){
				dataList.act.value =\"createTable\";
			}
			return result;
		}
		function doNewDir(){
			if(isNaN(dataList.dirName.value)){
				dataList.act.value =\"createDir\";
				return true;
			}
			return false;
		}
		function doDeleteDir(){
			document.dataList.act.value=\"deleteDir\"
			return confirm(\"Do you really want to delete this directory?\");
		}
		</script>
		<form name=\"dataList\" method=\"post\" action=\"?directory,$encodeDir\">
		<input type=\"hidden\" name=\"act\" value=\"\">
		<b class=\"title\" onclick=\"switchNav();\">Directory:$dir</b>
		<table cellpadding=0 cellspacing=0 width=\"100%\" class=\"tbldata\">
			<thead>
				<tr class=\"head\">
					<td nowrap colspan=2>Create new directory:</td>
				</tr>
			<thead>
			<tbody>
				<tr>
					<td align='right' width=\"23%\">directory name :&nbsp;</td>
					<td><input name=\"dirName\" style=\"width:60%\">&nbsp;A-Za-z0-9</td>
				</tr>	
				<tr>
					<td></td>
					<td nowrap><input type=submit onclick='doNewDir()' value='Create' class=button hidefocus></td>
				</tr>
			</tbody>
			<thead>
				<tr class=\"head\">
					<td nowrap colspan=2>Create new table:</td>
				</tr>
			<thead>
			<tbody>
				<tr>
					<td align='right' width=\"15%\">table name :&nbsp;</td>
					<td><input name=\"tableName\" style=\"width:60%\" title='Input table name here.' value='Input table name here.' class=\"text\" onfocus=\"inputFocus(this)\" onblur=\"inputBlur(this)\">&nbsp;A-Za-z0-9</td>
				</tr>	
				<tr>
					<td align='right'>fields :&nbsp;</td>
					<td><input name=\"fields\" style=\"width:60%\" title=\"id,name,address,...\" value=\"id,name,address,...\" class=\"text\" onfocus=\"inputFocus(this)\" onblur=\"inputBlur(this)\">&nbsp;A-Za-z0-9,example:\"id,name,address\"</td>
				</tr>	
				<tr>
					<td></td>
					<td nowrap><input type=submit onclick='doNewTable()' value='Create' class=button hidefocus></td>
				</tr>
			</tbody>
			<!--
			<tr class=\"bar\">
				<td nowrap colspan=2>Other action:</td>
			</tr>
			<tr>
				<td align='right'>Delete directory :&nbsp;</td>
				<td nowrap><input type=submit onclick='doDeleteDir()' value='Delete' class=button hidefocus></td>
			</tr>-->
		</table>
		</form>";
	}
	/*
	** create table
	*****************/
	function createTable($dir,$encodeDir){
		global $set;
		if(isset($_POST['tableName']) && isset($_POST['fields'])){
			if(!$this->_check($_POST['tableName'])){
				print '<script>alert("Table name not allowed!")</script>';
				return;
			}
			$_POST['tableName'] = $dir.'/'.$_POST['tableName'];
			$_POST['fields'] = explode(',',$_POST['fields']);
			if(!$this->_check($_POST['fields'])){
				print '<script>alert("Fields name not allowed!")</script>';
				return;
			}
			$this->connect($set['database'],$set['tmp']);
			if($this->create($_POST['tableName'],$_POST['fields'])){
				print "<script>jump2('?navigation,$encodeDir',top.nav.nav);jump2('?browse,".urlencode($_POST['tableName'])."');</script>";
			}
		}
	}
	/*
	** create director
	********************/
	function createDir($dir){
		global $set;
		$dirName = str_replace(array('/','\\','?','*',':','|','<','>','"'),'',$_POST['dirName']);
		if($dirName != $_POST['dirName']){
			print "<script>alert('Directory name not allow!')</script>";
			return;
		}
		$newDirName = $set['database'].$dir.'/'.$dirName;
		mkdir($newDirName, 0700);
		$encodedNewDirName = urlencode($dir.'/'.$_POST['dirName']);
		print "<script>jump2('?navigation,$encodedNewDirName',top.nav.nav);jump2('?directory,$encodedNewDirName');</script>";
	}
}
new GUI();

/*
** unquotes
** if magic_quotes_gpc is ON in php.ini then invalidation
***********************************************************/
function unquotes($content){
	if (get_magic_quotes_gpc()) {
		if (is_array($content)) {
			foreach ($content as $key=>$value) {
				$content[$key] = unquotes($value);
			}
		} else {
			$content = stripslashes($content);
		}
	}
	return $content;
}
/*
** scanDirectory ~= scandir::PHP5
***********************************/
function scanDirectory($_directory){
	$_dirHandle = @opendir($_directory.'/');
	$_result = array('directory'=>array(),'file'=>array());
	while(($_file = @readdir($_dirHandle)) !== false){
		$_file == '.' || $_file == '..' || $_result[is_dir($_directory.$_file)?'directory':'file'][] = $_file;
	}
	@closedir($_dirHandle);
	return $_result;
}